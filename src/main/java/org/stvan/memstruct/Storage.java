package org.stvan.memstruct;

import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.stvan.utils.fileparser.xml.XMLFile;
import org.stvan.utils.fileparser.xml.XMLNode;
import org.stvan.utils.fileparser.xml.exceptions.XMLNoSuchAttributeException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;

import java.util.ArrayList;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 26.08.2015
 */
public class Storage {
    private FileInfo root;
    private ArrayList<FileInfo> allInfo = new ArrayList<>();

    public Storage() {
    }

    public void addFileInfo(FileInfo fileInfo) {
        if (root == null) root = fileInfo;
        allInfo.add(fileInfo);
    }

    public FileInfo getRoot() {
        return root;
    }

    public ArrayList<FileInfo> getAllInfo() {
        return allInfo;
    }

    public void scan(String path, WildcardFileFilter filter) {
        scanFolder(new File(path), filter);
    }

    private FileInfo scanFolder(File folder, WildcardFileFilter filter) {
        FileInfo folderInfo = new FileInfo(folder);
        addFileInfo(folderInfo);
        File[] files = folder.listFiles((FilenameFilter) filter);
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    folderInfo.addChild(scanFolder(file, filter));
                } else {
                    FileInfo fileInfo = new FileInfo(file);
                    folderInfo.addChild(fileInfo);
                    addFileInfo(fileInfo);
                }
            }
        }
        return folderInfo;
    }

    public void save(String path) {
        XMLFile xmlFile = new XMLFile();
        xmlFile.setFile(new File(path));
        xmlFile.setNodeName("storage");
        xmlFile.addChild(saveFileInfo(root));
        xmlFile.save();
    }

    private XMLNode saveFileInfo(FileInfo folder) {
        XMLNode folderNode = folder.getXML();
        if (folder.hasChildren()) {
            for (FileInfo fileInfo : folder.getChildren()) {
                folderNode.addChild(saveFileInfo(fileInfo));
            }
        }
        return folderNode;
    }

    public void load(String path) throws FileNotFoundException, XMLNoSuchAttributeException {
        XMLFile xmlFile = new XMLFile(path);
        loadFileInfo(xmlFile.getFirstChild("folder"));
    }

    private FileInfo loadFileInfo(XMLNode folder) throws XMLNoSuchAttributeException {
        FileInfo folderInfo = new FileInfo(folder);
        addFileInfo(folderInfo);
        ArrayList<XMLNode> children = folder.getChildren();
        if (children != null) {
            for (XMLNode node : children) {
                if (node.getNodeName().equals("folder")) {
                    folderInfo.addChild(loadFileInfo(node));
                } else {
                    FileInfo fileInfo = new FileInfo(node);
                    folderInfo.addChild(fileInfo);
                    addFileInfo(fileInfo);
                }
            }
        }
        return folderInfo;
    }

    public void clear() {
        root = null;
        allInfo = new ArrayList<>();
    }
}
