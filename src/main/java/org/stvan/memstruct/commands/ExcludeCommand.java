package org.stvan.memstruct.commands;


import org.apache.commons.io.IOCase;
import org.stvan.memstruct.Console;
import org.stvan.memstruct.filters.ExcludeFileFilter;

import java.util.ArrayList;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 01.09.2015
 */
public class ExcludeCommand extends Command {
    ArrayList<String> wildCrds = new ArrayList<>();

    @Override
    public void addArg(int num, String arg) {
        if (arg == null) return;
        wildCrds.add(arg);
    }

    @Override
    public void run(Console console) {

    }

    public ExcludeFileFilter getFilter() {
        return new ExcludeFileFilter(wildCrds, IOCase.INSENSITIVE);
    }
}
