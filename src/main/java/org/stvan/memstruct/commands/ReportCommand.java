package org.stvan.memstruct.commands;

import org.stvan.memstruct.Console;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 31.08.2015
 */
public class ReportCommand extends Command {
    public String path;

    @Override
    public void addArg(int num, String arg) {
        path = arg;
    }

    @Override
    public void run(Console console) {

    }
}
