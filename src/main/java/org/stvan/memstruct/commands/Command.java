package org.stvan.memstruct.commands;

import org.joda.time.DateTime;
import org.stvan.memstruct.Console;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 28.08.2015
 */
public abstract class Command {

    public abstract void addArg(int num, String arg);

    public abstract void run(Console console);

    public static Command createCommand(String arg) {
        switch (arg) {
            case "-scan": {
                return new ScanCommand();
            }
            case "-save": {
                return new SaveCommand();
            }
            case "-load": {
                return new LoadCommand();
            }
            case "-compare": {
                return new CompareCommand();
            }
            case "-report": {
                return new ReportCommand();
            }
            case "-exclude": {
                return new ExcludeCommand();
            }
        }
        return null;
    }

    public static String filterDateTeamplate(String path) {
        if (path == null) return null;
        if (path.contains("#")) {
            String dateFormat = path.substring(path.indexOf("#") + 1, path.lastIndexOf("#"));
            return path.replace("#" + dateFormat + "#", DateTime.now().toString(dateFormat));
        }
        return path;
    }
}
