package org.stvan.memstruct.commands;

import org.stvan.memstruct.Console;
import org.stvan.memstruct.Storage;
import org.stvan.utils.fileparser.xml.exceptions.XMLNoSuchAttributeException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.UUID;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 28.08.2015
 */
public class LoadCommand extends Command {
    public String storName;
    public String path;

    @Override
    public void addArg(int num, String arg) {
        if (num == 0) {
            path = arg;
        } else if (num == 1) {
            storName = arg;
        }
    }

    @Override
    public void run(Console console) {
        File file = new File(path);
        if (!file.exists()) {
            throw new IllegalArgumentException("File does not exist -load " + path);
        }
        if (storName == null) {
            if (console.lastStor == null) {
                storName = UUID.randomUUID().toString();
            } else {
                storName = console.lastStor;
            }
        }
        console.lastStor = storName;
        Storage storage = new Storage();
        try {
            path = filterDateTeamplate(path);
            storage.load(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLNoSuchAttributeException e) {
            e.printStackTrace();
        }
        console.storages.put(storName, storage);
        System.out.println(new Date() + " Loaded " + path + " to " + storName);
    }
}
