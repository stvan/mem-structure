package org.stvan.memstruct.commands;

import org.stvan.memstruct.Console;
import org.stvan.memstruct.Storage;

import java.io.File;
import java.util.Date;
import java.util.UUID;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 28.08.2015
 */
public class ScanCommand extends Command {
    public String storName;
    public String path;

    @Override
    public void addArg(int num, String arg) {
        if (num == 0) {
            path = arg;
        } else if (num == 1) {
            storName = arg;
        }
    }

    @Override
    public void run(Console console) {
        File file = new File(path);
        if (!file.exists()) {
            throw new IllegalArgumentException("File does not exist -scan " + path);
        }
        if (storName == null) {
            if (console.lastStor == null) {
                storName = UUID.randomUUID().toString();
            } else {
                storName = console.lastStor;
            }
        }
        console.lastStor = storName;
        Storage storage = new Storage();
        storage.scan(path, console.getExcludeFilter());
        console.storages.put(storName, storage);
        System.out.println(new Date() + " Scanned " + path + " to " + storName);
    }
}
