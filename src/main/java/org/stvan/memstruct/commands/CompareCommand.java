package org.stvan.memstruct.commands;

import org.stvan.memstruct.Console;
import org.stvan.memstruct.FileInfo;
import org.stvan.memstruct.Storage;
import org.stvan.memstruct.StorageWorker;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 28.08.2015
 */
public class CompareCommand extends Command {
    public String oldStorageName;
    public String newStorageName;

    @Override
    public void addArg(int num, String arg) {
        if (num == 0) {
            oldStorageName = arg;
        } else if (num == 1) {
            newStorageName = arg;
        }
    }

    @Override
    public void run(Console console) {
        System.out.println();
        System.out.println("-----------------  Сравнение  ------------------");
        Storage oldStorage = console.storages.get(oldStorageName);
        Storage newStorage = console.storages.get(newStorageName);
        StorageWorker.compare(oldStorage, newStorage);

        ArrayList<FileInfo> results = new ArrayList<>();
        for (FileInfo fileInfo : oldStorage.getAllInfo()) {
            if (fileInfo.getCondition() != null) {
                results.add(fileInfo);
            }
        }

        for (FileInfo fileInfo : newStorage.getAllInfo()) {
            if (fileInfo.getCondition() != null) {
                if (fileInfo.getCondition().isMoved() || fileInfo.getCondition().isChanged()) continue;
                results.add(fileInfo);
            }
        }

        Collections.sort(results);
        long added = 0;
        long deleted = 0;
        for (FileInfo result : results) {
            if (result.getCondition().isAdded()) {
                added += result.getSize();
            } else if (result.getCondition().isDeleted()) {
                deleted += result.getSize();
            }
            StringBuilder reportStr = new StringBuilder(result.getCondition().toString());
            reportStr.append(" ");
            reportStr.append(result.toString().replace(newStorage.getRoot().getFullPath() + File.separator, ""));
            if (result.getCondition().isMoved()) {
                reportStr.append(" ");
                reportStr.append(result.getCandidate().toString().replace(newStorage.getRoot().getFullPath() + File.separator, ""));
            }
            reportStr.append(" ");
            reportStr.append(FileInfo.getReadableSize(result.getSize()));
            System.out.println(reportStr.toString());
        }
        System.out.println();

        System.out.println("Added: " + FileInfo.getReadableSize(added));
        System.out.println("Deleted: " + FileInfo.getReadableSize(deleted));
        System.out.println();

        System.out.println(new Date() + " Compared " + oldStorageName + " width " + newStorageName);
    }
}
