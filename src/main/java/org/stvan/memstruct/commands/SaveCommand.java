package org.stvan.memstruct.commands;

import org.joda.time.DateTime;
import org.stvan.memstruct.Console;
import org.stvan.memstruct.Storage;

import java.util.Date;
import java.util.UUID;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 28.08.2015
 */
public class SaveCommand extends Command {
    public String storName;
    public String path;

    @Override
    public void addArg(int num, String arg) {
        if (num == 0) {
            path = arg;
        } else if (num == 1) {
            storName = arg;
        }
    }

    @Override
    public void run(Console console) {
        if (storName == null) {
            if (console.lastStor == null) {
                storName = UUID.randomUUID().toString();
            } else {
                storName = console.lastStor;
            }
        }
        console.lastStor = storName;
        Storage storage = console.storages.get(storName);
        if (storage == null) throw new IllegalArgumentException("Storage does not exist -save " + storName);
        path = filterDateTeamplate(path);
        storage.save(path);
        System.out.println(new Date() + " Saved " + storName + " to " + path);
    }
}
