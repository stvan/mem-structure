package org.stvan.memstruct;

import java.util.ArrayList;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 26.08.2015
 */
public class StorageWorker {
    public static int ANY = 0;
    public static int FOLDER = 1;
    public static int FILE = 2;

    public static ArrayList<FileInfo> findFoldersByName(Storage storage, String name) {
        return findInfoByName(storage, name, FOLDER);
    }

    public static ArrayList<FileInfo> findFilesByName(Storage storage, String name) {
        return findInfoByName(storage, name, FILE);
    }

    public static ArrayList<FileInfo> findInfoByName(Storage storage, String name) {
        return findInfoByName(storage, name, ANY);
    }

    public static ArrayList<FileInfo> findInfoByName(Storage storage, String name, int type) {
        ArrayList<FileInfo> res = new ArrayList<>();
        for (FileInfo fileInfo : storage.getAllInfo()) {
            if (name.equals(fileInfo.getName())) {
                if (type == FOLDER && fileInfo.isFolder()) {
                    res.add(fileInfo);
                } else if (type == FILE && !fileInfo.isFolder()) {
                    res.add(fileInfo);
                } else if (type == ANY) {
                    res.add(fileInfo);
                }
            }
        }
        return res;
    }

    public static boolean contains(Storage storage, FileInfo fileInfo) {
        return storage.getAllInfo().contains(fileInfo);
    }

    public static FileInfo get(Storage storage, FileInfo fileInfo) {
        ArrayList<FileInfo> all = storage.getAllInfo();
        int index = all.indexOf(fileInfo);
        if (index == -1) return null;
        return all.get(index);
    }

    public static void compare(Storage oldState, Storage newState) {
        checkOldFileInfo(oldState.getRoot(), newState);
        checkNewFileInfo(newState.getRoot(), oldState);
    }

    private static void checkOldFileInfo(FileInfo fileInfo, Storage newState) {
        FileInfo newFileInfo = get(newState, fileInfo);
        if (newFileInfo == null) {
            if (findCandidate(fileInfo, newState)) {
                fileInfo.setCondition(new Condition(Condition.MOVED));
                fileInfo.getCandidate().setCandidate(fileInfo);
                fileInfo.getCandidate().setCondition(new Condition(Condition.MOVED));
            } else {
                fileInfo.setCondition(new Condition(Condition.DELETED));
            }
        } else {
            if (!fileInfo.isFolder() && newFileInfo.getSize() != fileInfo.getSize()) {
                fileInfo.setCondition(new Condition(Condition.CHANGED));
                newFileInfo.setCondition(new Condition(Condition.CHANGED));
            }
            if (fileInfo.hasChildren()) {
                for (FileInfo info : fileInfo.getChildren()) {
                    checkOldFileInfo(info, newState);
                }
            }
        }
    }

    private static void checkNewFileInfo(FileInfo fileInfo, Storage oldState) {
        if (fileInfo.getCondition() != null) return;
        if (!contains(oldState, fileInfo)) {
            fileInfo.setCondition(new Condition(Condition.ADDED));
        } else {
            if (fileInfo.hasChildren()) {
                for (FileInfo info : fileInfo.getChildren()) {
                    checkNewFileInfo(info, oldState);
                }
            }
        }
    }

    private static boolean findCandidate(FileInfo fileInfo, Storage storage) {
        ArrayList<FileInfo> candidates = findInfoByName(storage, fileInfo.getName());
        if (candidates.isEmpty()) return false;
        if (candidates.size() == 1) {
            fileInfo.setCandidate(candidates.get(0));
            return true;
        }
        for (FileInfo candidate : candidates) {
            if (candidate.getSize() == fileInfo.getSize()) {
                fileInfo.setCandidate(candidate);
                return true;
            }
        }
        return false;
    }
}
