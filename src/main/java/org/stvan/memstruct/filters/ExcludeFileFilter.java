package org.stvan.memstruct.filters;

import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.File;
import java.util.List;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 01.09.2015
 */
public class ExcludeFileFilter extends WildcardFileFilter{
    public ExcludeFileFilter(List<String> wildcards, IOCase caseSensitivity) {
        super(wildcards, caseSensitivity);
    }

    @Override
    public boolean accept(File file) {
        return !super.accept(file);
    }

    @Override
    public boolean accept(File dir, String name) {
        return !super.accept(dir, name);
    }
}
