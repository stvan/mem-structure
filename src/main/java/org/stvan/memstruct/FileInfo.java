package org.stvan.memstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.joda.time.DateTime;
import org.stvan.utils.fileparser.xml.XMLAttribute;
import org.stvan.utils.fileparser.xml.XMLNode;
import org.stvan.utils.fileparser.xml.exceptions.XMLNoSuchAttributeException;

import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.attribute.DosFileAttributes;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 26.08.2015
 */
public class FileInfo implements Comparable<FileInfo> {
    private boolean folder = false;
    private String path;
    private String name;
    private String extension;
    private long size;
    private FileInfo parent;
    private ArrayList<FileInfo> children;
    private DateTime createDate;
    private DateTime modifyDate;
    private boolean higgen = false;
    private boolean system = false;

    //Для проверки
    private Condition condition;
    private FileInfo candidate;

    public FileInfo() {
    }

    public FileInfo(File file) {
        if (file.isDirectory()) {
            setFolder(true);
        } else {
            setSize(FileUtils.sizeOf(file));
            setExtension(FilenameUtils.getExtension(file.getName()));
        }

        setName(FilenameUtils.getBaseName(file.getName()));

        if (file.getParentFile() != null) {
            setPath(file.getParentFile().getPath());
        } else {
            setPath(file.getPath());
        }

        try {
            DosFileAttributes fileAttributes = Files.readAttributes(file.toPath(), DosFileAttributes.class);
            createDate = new DateTime(fileAttributes.creationTime().toMillis());
            modifyDate = new DateTime(fileAttributes.lastModifiedTime().toMillis());
            higgen = fileAttributes.isHidden();
            system = fileAttributes.isSystem();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public FileInfo(XMLNode node) throws XMLNoSuchAttributeException {
        if (node.getNodeName().equals("folder")) {
            folder = true;
        }
        path = node.getStringAttr("path");
        name = node.getStringAttr("name");
        if (node.hasAttribute("extension")) extension = node.getStringAttr("extension");
        if (node.hasAttribute("size")) size = node.getIntAttr("size");
        if (node.hasAttribute("crD")) createDate = new DateTime(Long.valueOf(node.getStringAttr("crD")));
        if (node.hasAttribute("mD")) modifyDate = new DateTime(Long.valueOf(node.getStringAttr("mD")));
        if (node.hasAttribute("higgen")) higgen = true;
        if (node.hasAttribute("system")) system = true;
    }

    public XMLNode getXML() {
        XMLNode node = new XMLNode();
        if (isFolder()) {
            node.setNodeName("folder");
        } else {
            node.setNodeName("file");
        }
        node.addAttribute(new XMLAttribute("path", path));
        node.addAttribute(new XMLAttribute("name", name));
        if (extension != null) node.addAttribute(new XMLAttribute("extension", extension));
        if (!isFolder()) {
            node.addAttribute(new XMLAttribute("size", String.valueOf(size)));
        }
        if (createDate != null) node.addAttribute(new XMLAttribute("crD", String.valueOf(createDate.getMillis())));
        if (modifyDate != null) node.addAttribute(new XMLAttribute("mD", String.valueOf(modifyDate.getMillis())));
        if (higgen) node.addAttribute(new XMLAttribute("higgen", ""));
        if (system) node.addAttribute(new XMLAttribute("system", ""));
        return node;
    }

    public void addChild(FileInfo child) {
        if (children == null) children = new ArrayList<>();
        child.setParent(this);
        children.add(child);
        Collections.sort(children);
        size += child.getSize();
    }

    public boolean hasChildren() {
        return !(children == null || children.isEmpty());
    }

    public ArrayList<FileInfo> getChildren() {
        return children;
    }

    public boolean isFolder() {
        return folder;
    }

    public void setFolder(boolean folder) {
        this.folder = folder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public double getGigabyteSize() {
        return getSize() / 1073741824d;
    }

    public static String getReadableSize(long sourceSize) {
        if (sourceSize < 1024) return sourceSize + " B";
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        NumberFormat formatter = new DecimalFormat("#0.0", symbols);
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        double size = sourceSize / 1024d;
        if (size < 1024) return formatter.format(size) + " KB";
        size = size / 1024d;
        if (size < 1024) return formatter.format(size) + " MB";
        size = size / 1024d;
        if (size < 1024) return formatter.format(size) + " GB";
        size = size / 1024d;
        return formatter.format(size) + "TB";
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public FileInfo getParent() {
        return parent;
    }

    public void setParent(FileInfo parent) {
        this.parent = parent;
    }

    public String getFullPath() {
        StringBuilder builder = new StringBuilder(path);
        if (name != null) {
            builder.append(File.separator).append(name);
        }
        if (extension != null) {
            builder.append(".").append(extension);
        }
        return builder.toString();
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public FileInfo getCandidate() {
        return candidate;
    }

    public void setCandidate(FileInfo candidate) {
        this.candidate = candidate;
    }

    @Override
    public String toString() {
        return getFullPath();
    }

    @Override
    public int compareTo(FileInfo o) {
        int res = getPath().compareTo(o.getPath());
        if (res != 0) return res;
        if (isFolder() != o.isFolder()) {
            if (isFolder()) {
                return -1;
            }
            return 1;
        }
        res = getName().compareTo(o.getName());
        if (res != 0) return res;
        return Condition.compare(condition, o.getCondition());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileInfo fileInfo = (FileInfo) o;

        if (folder != fileInfo.folder) return false;
        if (!path.equals(fileInfo.path)) return false;
        if (!name.equals(fileInfo.name)) return false;
        return !(extension != null ? !extension.equals(fileInfo.extension) : fileInfo.extension != null);
    }

    @Override
    public int hashCode() {
        int result = (folder ? 1 : 0);
        result = 31 * result + path.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (extension != null ? extension.hashCode() : 0);
        return result;
    }
}
