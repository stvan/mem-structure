package org.stvan.memstruct;

import org.stvan.utils.fileparser.xml.exceptions.XMLNoSuchAttributeException;

import java.io.FileNotFoundException;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 26.08.2015
 */
public class Main {
    private Storage storage;

    public static void main(String[] args) {
        if (args.length == 0) {
            new Main();
        } else {
            new Console(args);
        }
    }

    public Main() {
//        storage = new Storage();
//        //storage.scan("D:/Temp");
//        //storage.save("D:/storage.xml");
//        try {
//            storage.load("D:/storage.xml");
//            Storage storageNew = new Storage();
//            storageNew.scan("D:/Temp");
//            StorageWorker.compare(storage, storageNew);
//            for (FileInfo fileInfo : storage.getAllInfo()) {
//                if (fileInfo.getState() != null) {
//                    System.out.println(fileInfo + " " + fileInfo.getState());
//                }
//            }
//            for (FileInfo fileInfo : storageNew.getAllInfo()) {
//                if (fileInfo.getState() != null) {
//                    System.out.println(fileInfo + " " + fileInfo.getState());
//                }
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (XMLNoSuchAttributeException e) {
//            e.printStackTrace();
//        }
    }
}
