package org.stvan.memstruct;

import org.stvan.memstruct.commands.Command;
import org.stvan.memstruct.commands.ExcludeCommand;
import org.stvan.memstruct.commands.ReportCommand;
import org.stvan.memstruct.commands.ScanCommand;
import org.stvan.memstruct.filters.ExcludeFileFilter;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 28.08.2015
 */
public class Console {
    public String lastStor;
    public HashMap<String, Storage> storages = new HashMap<>();
    private ArrayList<Command> commands = new ArrayList<>();
    private ReportCommand reportCommand;
    private ExcludeFileFilter excludeFilter;

    public Console(String[] args) {
        Command command = null;
        int num = 0;
        ExcludeCommand excludeCommand = null;
        for (String arg : args) {
            if (arg.startsWith("-")) {
                command = ScanCommand.createCommand(arg);
                if (command == null) throw new IllegalArgumentException("Illegal command " + arg);
                if (command instanceof ReportCommand) {
                    reportCommand = (ReportCommand) command;
                    continue;
                } else if (command instanceof ExcludeCommand) {
                    excludeCommand = (ExcludeCommand) command;
                    continue;
                }
                commands.add(command);
                num = 0;
            } else {
                assert command != null;
                command.addArg(num, arg);
                num++;
            }
        }
        if (excludeCommand != null) {
            excludeFilter = excludeCommand.getFilter();
        }

        if (reportCommand != null) {
            if (reportCommand.path == null) {
                reportCommand.path = "report.txt";
            } else {
                reportCommand.path = Command.filterDateTeamplate(reportCommand.path);
            }
            try {
                OutputStream out = new FileOutputStream(new File(reportCommand.path), true);
                System.setOut(new PrintStream(out));
                System.setErr(new PrintStream(out));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        System.out.println("------------------------------  " + new Date() + "  ------------------------------");
        for (Command comm : commands) {
            comm.run(this);
        }
        System.out.println("------------------------------  " + new Date() + "  ------------------------------");
    }

    public ExcludeFileFilter getExcludeFilter() {
        return excludeFilter;
    }
}
