package org.stvan.memstruct;

/**
 * Author: Arkadiy V. Klmanskiy (stvan@mail.ru)
 * Date: 31.08.2015
 */
public class Condition implements Comparable<Condition> {
    public static int CHANGED = 1;
    public static int ADDED = 2;
    public static int MOVED = 3;
    public static int DELETED = 4;
    private int condition;

    public Condition(int condition) {
        this.condition = condition;
    }

    public boolean isChanged() {
        return condition == CHANGED;
    }

    public boolean isAdded() {
        return condition == ADDED;
    }

    public boolean isMoved() {
        return condition == MOVED;
    }

    public boolean isDeleted() {
        return condition == DELETED;
    }

    @Override
    public String toString() {
        if (isChanged()) return "Изменен";
        if (isAdded()) return "Добавлен";
        if (isMoved()) return "Перемещен";
        if (isDeleted()) return "Удален";
        return null;
    }

    @Override
    public int compareTo(Condition o) {
        return Condition.compare(this, o);
    }

    public static int compare(Condition c1, Condition c2) {
        if (c1 == null && c2 == null) return 0;
        if (c1 == null) return 1;
        if (c2 == null) return -1;
        return Integer.compare(c1.condition, c2.condition);
    }
}
